package com.example.servicenow.jms;

import com.example.servicenow.persistence.dtos.TicketDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

@Service
public class JmsTicketMessagingProducerService {
    private static Logger log = LoggerFactory.getLogger(JmsTicketMessagingProducerService.class);
    private JmsTemplate jmsTemplate;
    private Destination destination;
    private MessageConverter jacksonJmsMessageConverter;


    @Autowired
    public JmsTicketMessagingProducerService(JmsTemplate jmsTemplate, Destination destination, MessageConverter jacksonJmsMessageConverter) {
        this.jmsTemplate = jmsTemplate;
        this.destination = destination;
        this.jacksonJmsMessageConverter = jacksonJmsMessageConverter;
    }

    public void sendNewTicket(TicketDTO ticketDTO) {
        log.info("Sending a new ticket to Aggregate Service {} ", ticketDTO);
        jmsTemplate.convertAndSend(destination, ticketDTO);
    }
}
