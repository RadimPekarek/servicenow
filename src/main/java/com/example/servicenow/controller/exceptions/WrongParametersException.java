package com.example.servicenow.controller.exceptions;

public class WrongParametersException extends RuntimeException {

    public WrongParametersException() {
    }

    public WrongParametersException(String message) {
        super(message);
    }

    public WrongParametersException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongParametersException(Throwable cause) {
        super(cause);
    }

    public WrongParametersException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
