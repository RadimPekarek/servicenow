package com.example.servicenow.controller;

import com.example.servicenow.controller.exceptions.EntityNotFoundException;
import com.example.servicenow.controller.exceptions.TicketNotCreatedException;
import com.example.servicenow.controller.exceptions.TicketNotFoundException;
import com.example.servicenow.controller.exceptions.WrongParametersException;
import com.example.servicenow.persistence.dtos.TicketCreateDTO;
import com.example.servicenow.persistence.dtos.TicketDTO;
import com.example.servicenow.persistence.entities.Ticket;
import com.example.servicenow.services.TicketService;
import com.example.servicenow.services.exceptions.ServiceLayerException;
import com.example.servicenow.utils.ParametersChecker;
import com.example.servicenow.utils.annotations.ApiPageableSwagger;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.net.URI;

@RestController
@RequestMapping("/tickets")
@Api(value = "/tickets", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class TicketController {
    private static Logger log = LoggerFactory.getLogger(TicketController.class);
    private TicketService ticketService;
    private ParametersChecker checker;


    @Autowired
    public TicketController(TicketService ticketService, ParametersChecker checker) {
        this.ticketService = ticketService;
        this.checker = checker;

    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get a ticket by Id",
            response = TicketDTO.class,
            nickname = "getTicketById",
            produces = "application/json"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved ticket"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    public TicketDTO getTicketById(
            @ApiParam(name = "id", value = "Ticket id to retrieve ticket", required = true)
            @PathVariable("id") @Min(1) Long id) {
        try {
            return ticketService.getTicketById(id).orElseThrow(() -> new TicketNotFoundException("No ticket with id " + id + " found!"));
        } catch (ServiceLayerException exception) {
            throw new javax.persistence.EntityNotFoundException(exception.getLocalizedMessage());
        }
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            httpMethod = "POST",
            value = "Create Ticket",
            response = TicketDTO.class,
            nickname = "createPerson",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "The requested resource was not found.")
    })
    public ResponseEntity<?> create(
            @ApiParam(value = "Ticket to be created")
            @RequestBody @Valid TicketCreateDTO ticket) {
        try {
            TicketDTO ticketSaved = ticketService.save(ticket);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(ticketSaved.getId()).toUri();
            return ResponseEntity.created(location).build();
        } catch (ServiceLayerException exception) {
            throw new TicketNotCreatedException(exception);
        }
    }


    @ApiOperation(
            httpMethod = "GET",
            value = "Get All Tickets.",
            response = TicketDTO.class,
            responseContainer = "Page",
            nickname = "getAllTickets",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "The requested resource was not found.")
    })
    @ApiPageableSwagger
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public Page<TicketDTO> getAllTickets(@QuerydslPredicate(root = Ticket.class) Predicate predicate,
                                         Pageable pageable,
                                         @ApiParam(value = "Parameters for QueryDSL filtering", required = false)
                                         @RequestParam MultiValueMap<String, String> parameters
    ) {
        try {
            checker.setClazz(TicketDTO.class);
            checker.setParameters(parameters.keySet());
            if (checker.matching()) return ticketService.findAll(predicate, pageable);
            else
                throw new WrongParametersException("Request params should be "
                        + checker.getCorrectFieldNames()
                        .toString().replaceAll("\\[(.*?)\\]", "$1")
                        + ".");
        } catch (ServiceLayerException exception) {
            throw new EntityNotFoundException(TicketDTO.class, parameters.keySet().toArray(new String[0]));
        }
    }


}
