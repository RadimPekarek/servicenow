package com.example.servicenow.controller.config;

import com.example.servicenow.controller.exceptions.model.ApiErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebErrorConfiguration {

    @Bean
    public ErrorAttributes errorAttributes() {
        return new ApiErrorAttributes(true);
    }

}
