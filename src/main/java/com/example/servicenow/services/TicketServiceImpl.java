package com.example.servicenow.services;

import com.example.servicenow.jms.JmsTicketMessagingProducerService;
import com.example.servicenow.persistence.dtos.TicketCreateDTO;
import com.example.servicenow.persistence.dtos.TicketDTO;
import com.example.servicenow.persistence.entities.Ticket;
import com.example.servicenow.persistence.repositories.TicketJpaRepository;
import com.example.servicenow.services.exceptions.ServiceLayerException;
import com.querydsl.core.types.Predicate;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {
    private TicketJpaRepository ticketRepository;
    private JmsTicketMessagingProducerService messagingProducerService;

    @Autowired
    public TicketServiceImpl(TicketJpaRepository ticketRepository, JmsTicketMessagingProducerService messagingProducerService) {
        this.ticketRepository = ticketRepository;
        this.messagingProducerService = messagingProducerService;
    }

    @Override
    public Optional<TicketDTO> getTicketById(Long id) {
        try {
            Optional<Ticket> ticket = ticketRepository.getTicketById(id);
            return ticket.map(this::convertToTicketDto);
        } catch (HibernateException ex) {
            throw new ServiceLayerException(ex);
        }
    }

    @Override
    public TicketDTO save(TicketCreateDTO ticketDTO) {
        try {
            TicketDTO saved = null;
            if (ticketDTO != null) {
                Ticket ticket = convertToTicket(ticketDTO);
                ticket = ticketRepository.save(ticket);
                saved = convertToTicketDto(ticket);
            }
            messagingProducerService.sendNewTicket(saved);
            return saved;
        } catch (HibernateException ex) {
            throw new ServiceLayerException(ex);
        }
    }

    @Override
    public Page<TicketDTO> findAll(Predicate predicate, Pageable pageable) {
        try {
            Page<Ticket> tickets = ticketRepository.findAll(predicate, pageable);
            return tickets.map(this::convertToTicketDto);
        } catch (HibernateException ex) {
            throw new ServiceLayerException(ex);
        }
    }

    private TicketDTO convertToTicketDto(final Ticket ticket) {
        final TicketDTO ticketDTO = new TicketDTO();
        ticketDTO.setId(ticket.getId());
        ticketDTO.setName(ticket.getName());
        ticketDTO.setEmail(ticket.getEmail());
        ticketDTO.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketDTO.setIdPersonAssigned(ticket.getIdPersonAssigned());
        ticketDTO.setCreationDateTime(ticket.getCreationDateTime());
        return ticketDTO;
    }

    private Ticket convertToTicket(final TicketCreateDTO ticketDTO) {
        final Ticket ticket = new Ticket();
        ticket.setId(null);
        ticket.setName(ticketDTO.getName());
        ticket.setEmail(ticketDTO.getEmail());
        ticket.setIdPersonCreator(ticketDTO.getIdPersonCreator());
        ticket.setIdPersonAssigned(ticketDTO.getIdPersonAssigned());
        ticket.setCreationDateTime(ticketDTO.getCreationDateTime());
        return ticket;
    }
}
