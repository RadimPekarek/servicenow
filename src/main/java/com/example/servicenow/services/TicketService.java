package com.example.servicenow.services;

import com.example.servicenow.persistence.dtos.TicketCreateDTO;
import com.example.servicenow.persistence.dtos.TicketDTO;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface TicketService {
    Optional<TicketDTO> getTicketById(Long id);

    TicketDTO save(TicketCreateDTO ticket);

    Page<TicketDTO> findAll(Predicate predicate, Pageable pageable);

}
