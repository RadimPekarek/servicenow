package com.example.servicenow.persistence.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@ApiModel(value = "TicketDTO", description = "Information about ticket.")
public class TicketDTO implements Serializable {

    private static final long serialVersionUID = -295422703255886286L;

    @ApiModelProperty(value = "Ticket id.", example = "1", notes = "The database generated ticket ID")
    private Long id;
    @ApiModelProperty(value = "Ticket name", example = "rolling incident")
    private String name;
    @ApiModelProperty(value = "Ticket email.", example = "radek.kruta@seznam.czt", notes = "The email of person who created the ticket")
    private String email;
    @ApiModelProperty(value = "Id of person.", example = "1", notes = "The id of person who created the ticket")
    private Long idPersonCreator;
    @ApiModelProperty(value = "Id of person.", example = "2", notes = "The id of person to whom the ticket was assigned")
    private Long idPersonAssigned;
    @ApiModelProperty(value = "Ticket timestamp.", example = "2020-02-19T11:56:13.89608", notes = "The timestamp when the ticket was created")
    private LocalDateTime creationDateTime;

    public TicketDTO() {
    }

    public TicketDTO(String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime) {
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
    }

    public TicketDTO(Long id, String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TicketDTO)) return false;
        TicketDTO ticketDTO = (TicketDTO) o;
        return Objects.equals(getId(), ticketDTO.getId()) &&
                Objects.equals(getName(), ticketDTO.getName()) &&
                Objects.equals(getEmail(), ticketDTO.getEmail()) &&
                Objects.equals(getIdPersonCreator(), ticketDTO.getIdPersonCreator()) &&
                Objects.equals(getIdPersonAssigned(), ticketDTO.getIdPersonAssigned()) &&
                Objects.equals(getCreationDateTime(), ticketDTO.getCreationDateTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getIdPersonCreator(), getIdPersonAssigned(), getCreationDateTime());
    }

    @Override
    public String toString() {
        return "TicketDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDateTime=" + creationDateTime +
                '}';
    }
}
