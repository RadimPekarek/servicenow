package com.example.servicenow.persistence.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@ApiModel(value = "TicketCreateDTO", description = "Ticket object to create in the database.")
public class TicketCreateDTO {

    @ApiModelProperty(value = "Ticket name", example = "rolling incident")
    private String name;
    @ApiModelProperty(value = "Ticket email.", example = "radek.kruta@seznam.czt", notes = "The email of person who created the ticket")
    @Email
    @NotNull
    @NotEmpty
    private String email;
    @ApiModelProperty(value = "Id of person.", example = "1", notes = "The id of person who created the ticket")
    @NotNull
    private Long idPersonCreator;
    @ApiModelProperty(value = "Id of person.", example = "2", notes = "The id of person to whom the ticket was assigned")
    @NotNull
    private Long idPersonAssigned;
    @ApiModelProperty(value = "Ticket timestamp.", example = "2020-02-19T11:56:13.89608", notes = "The timestamp when the ticket was created")
    private LocalDateTime creationDateTime;

    public TicketCreateDTO(String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime) {
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TicketCreateDTO)) return false;
        TicketCreateDTO that = (TicketCreateDTO) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getIdPersonCreator(), that.getIdPersonCreator()) &&
                Objects.equals(getIdPersonAssigned(), that.getIdPersonAssigned()) &&
                Objects.equals(getCreationDateTime(), that.getCreationDateTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getEmail(), getIdPersonCreator(), getIdPersonAssigned(), getCreationDateTime());
    }

    @Override
    public String toString() {
        return "TicketCreateDTO{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDateTime=" + creationDateTime +
                '}';
    }
}
