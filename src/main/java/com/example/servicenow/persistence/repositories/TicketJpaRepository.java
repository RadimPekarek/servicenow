package com.example.servicenow.persistence.repositories;

import com.example.servicenow.persistence.entities.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TicketJpaRepository extends PagingAndSortingRepository<Ticket, Long>, QuerydslPredicateExecutor<Ticket> {
    Optional<Ticket> getTicketById(Long id);

    Ticket save(Ticket ticket);

    Page<Ticket> findAll(Pageable pageable);
}
