package com.example.servicenow.persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
//@ApiModel(description = "All details about ServiceNow ticket. ")
public class Ticket implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    //  @ApiModelProperty(notes = "The database generated ticket ID")
    private Long id;
    //  @ApiModelProperty(notes = "The ticket name")
    private String name;
    //   @ApiModelProperty(notes = "The email of person who created the ticket")
    private String email;
    @Column(name = "id_person_creator")
    //  @ApiModelProperty(notes = "The id of person who created the ticket")
    private Long idPersonCreator;
    @Column(name = "id_person_assigned")
    //  @ApiModelProperty(notes = "The id of person to whom the ticket was assigned")
    private Long idPersonAssigned;
    @Column(name = "creation_datetime")
    //  @ApiModelProperty(notes = "The timestamp when the ticket was created")

    private LocalDateTime creationDateTime;

    public Ticket() {
    }

    public Ticket(String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime) {
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
    }

    public Ticket(Long id, String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(getId(), ticket.getId()) &&
                Objects.equals(getName(), ticket.getName()) &&
                Objects.equals(getEmail(), ticket.getEmail()) &&
                Objects.equals(getIdPersonCreator(), ticket.getIdPersonCreator()) &&
                Objects.equals(getIdPersonAssigned(), ticket.getIdPersonAssigned()) &&
                Objects.equals(getCreationDateTime(), ticket.getCreationDateTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getIdPersonCreator(), getIdPersonAssigned(), getCreationDateTime());
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDateTime=" + creationDateTime +
                '}';
    }
}
