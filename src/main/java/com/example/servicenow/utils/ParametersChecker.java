package com.example.servicenow.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class ParametersChecker {
    private Logger log = LoggerFactory.getLogger(ParametersChecker.class);
    private ObjectMapper objectMapper;
    private Class<?> clazz;
    private Set<String> parameters;

    @Autowired
    public ParametersChecker(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Set<String> getParameters() {
        return parameters;
    }

    public void setParameters(Set<String> parameters) {
        this.parameters = parameters;
    }

    public boolean matching() {
        if (clazz != null && !parameters.isEmpty()) {
            return getFieldNamesStream()
                    .anyMatch(field -> parameters.contains(field));
        } else return parameters.isEmpty();
    }

    public List<String> getCorrectFieldNames() {
        return getFieldNamesStream().collect(Collectors.toCollection(ArrayList::new));
    }

    private Stream<String> getFieldNamesStream() {
        Stream<String> fieldNames = Arrays.stream(clazz.getDeclaredFields()).map(Field::getName);
        return Stream.concat(fieldNames, Stream.of("page", "size", "sort"));
    }

}
