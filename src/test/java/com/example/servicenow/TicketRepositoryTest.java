package com.example.servicenow;

import com.example.servicenow.persistence.entities.Ticket;
import com.example.servicenow.persistence.repositories.TicketJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceNowApplication.class)
public class TicketRepositoryTest {
    private static Logger log = LoggerFactory.getLogger(TicketRepositoryTest.class);
    @Autowired
    TicketJpaRepository repository;

    @Test
    public void getTicketById() {
        Optional<Ticket> ticket = repository.getTicketById(1L);
        log.info("ticket {}", ticket);
        assertTrue(ticket.isPresent());

    }

/*    @Test
    public void getTicketByName() {
        Optional<Ticket> ticket = repository.getTicketByName("rolling incident");
        log.info("ticket {}", ticket);
        assertTrue(ticket.isPresent());

    }*/

}
